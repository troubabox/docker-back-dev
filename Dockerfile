FROM openjdk:8-slim-buster




RUN apt-get update && apt-get install -y curl gnupg2 wkhtmltopdf

RUN echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | tee /etc/apt/sources.list.d/sbt.list && \
echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | tee /etc/apt/sources.list.d/sbt_old.list && \
curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | apt-key add && \
apt-get update && apt-get install -y sbt 


RUN mkdir -p /data/annexes  /data/thumbnails && chmod -R a+rwx /data


ENV _JAVA_OPTIONS='-Duser.home=/var/home'

VOLUME /data/

EXPOSE 9000

WORKDIR /app

ENTRYPOINT ["sbt"]

